import React from "react";
import { Link } from "react-router-dom";
import IconFacebook from '../images/facebook.png';
import IconTwitter from '../images/twitter.png';
import IconYoutube from '../images/youtube.png';


function Footer() {
    return (
      <section id="footer" class="footer py-7">
        <div class="container">
          <div class="footer-top">
            <div class="icons mx-auto text-center">
              <a href="https://www.facebook.com/kryptonite.africa/"><img src={IconFacebook} class="img-fluid icon" alt="Icon" /></a>
              <a href="https://twitter.com/getkryptonite"><img src={IconTwitter} class="img-fluid icon" alt="Icon" /></a>
              <a href="https://www.youtube.com/channel/UCag_3ZsiTKFemrlaW8cU38A"><img src={IconYoutube} class="img-fluid icon" alt="Icon" /></a>
            </div>
          </div>
          <div class="row">
            <div class="footer-bottom">
            <div class="d-flex align-items-center col-lg-6">
              <p>Copyright © 2022, Tous Droits Réservés  | <Link to="/">Kryptonite.africa</Link></p>
            </div>
            <div class="col-lg-6 d-flex justify-content-end align-items-center">
              <ul>
                <li><a href="https://app.kryptonite.africa/faq">Comment ça marche?</a></li>
                <li><a href="https://app.kryptonite.africa/legal">Mentions légales</a></li>
                <li><a href="https://app.kryptonite.africa/cgu"> CGU</a></li>
                <li><a href="https://app.kryptonite.africa/support">Support</a></li>
              </ul>
            </div>
            </div>
          </div>
        </div>
      </section>
    );
}


export default Footer;