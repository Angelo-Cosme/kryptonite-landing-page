import React from "react";
import PlanetEarth from '../images/planet-earth.png';
import ExChange from '../images/dollar.png';
import Routine from '../images/routine.png';
import Hours from '../images/24-hours.png';

import Slider from "react-slick";
import 'slick-carousel/slick/slick.css'; 
import 'slick-carousel/slick/slick-theme.css';

export default function SimpleSlider() {
  var settings = {
    dots: false,
    infinite: true,
    speed: 10000,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <section id="setup" class="setup py-11">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="content-setup">
              <h2>Les quatre piliers de notre engagement</h2>
              <p>
                Découvrez les valeurs que nous prônons et défendons à travers 
                nos différents produits et offres de services.
              </p>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <Slider {...settings}>
                  <div class="col-sm-6">
                    <div class="inner">
                      <div class="icon"><img src={Routine} class="img-fluid" alt="Kryptonite-setup" /></div>
                      <h3>Vous faciliter la vie</h3>
                      <p>Que ce soit avec Kryptonite transfert ou exchange, notre objectif principal demeure intact.</p>
                      <div class="num">1</div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="inner">
                      <div class="icon"><img src={PlanetEarth} class="img-fluid" alt="Kryptonite-setup" /></div>
                      <h3>Un monde plus ouvert</h3>
                      <p>Nous pensons que tout le monde mérite de vivre dans un monde plus ouvert, plus prospère et plus juste.</p>
                      <div class="num">2</div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="inner">
                      <div class="icon"><img src={ExChange} class="img-fluid" alt="Kryptonite-setup" /></div>
                      <h3>Protéger votre argent</h3>
                      <p>Nous ne sommes prêts à accepter aucun compromis lorsqu’il s’agit de la sécurité de votre argent.</p>
                      <div class="num">3</div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="inner">
                      <div class="icon"><img src={Hours} class="img-fluid" alt="Kryptonite-setup" /></div>
                      <h3>Toujours disponible pour vous</h3>
                      <p>Conscients que l’argent est un sujet sensible, nous travaillons à être toujours disponible quand vous en avez besoin.</p>
                      <div class="num">4</div>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
        </div>
      </div>
    </section>
  );
}

