import React from "react";
import ImgBlockain from '../images/crypto-header.png';


function Blochain() {
    return (
      <section id="blockain" class="blockain py-11">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src={ImgBlockain} class="img-fluid" alt="Kryptonite-Blochain" />
          </div>
          <div class="col-md-6">
            <div class="content mt-7">
              <h2>La blockchain au service d’un monde sans frontières</h2>
              <p>
                Avec Kryptonite, entrez dans un monde où votre argent ne connaît 
                plus aucune limite. Transférez de l’argent d’un bout du monde à l’autre, 
                de façon rapide, sécurisée et sans aucune contrainte.
              </p>
              <p>
                Désormais, votre argent ira où vous avez besoin qu’il soit! Que ce soit pour aider 
                votre famille restée au pays, faire du business avec des partenaires à Dubai 
                ou simplement vous faire payer pour vos services, Kryptonite répond à tous ces besoins !
              </p>
              <a href="https://app.kryptonite.africa/transfert" class="btn">Envoyer de l’argent</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}


export default Blochain;