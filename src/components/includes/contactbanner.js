import React from "react";


import ImgContact from '../images/contact.png';

function ContactBanner () {
    return( 
        <section id="contact" class="contact" >
        <div class="container">
          <div class="row content">
            <div class="col-lg-6 col-md-8 col-sm-12 me-auto flex-column">
              <h1>Contact</h1>
                <p>Besoin d'aide ou de plus amples informations? Un service clientèle disponible 24h/24
                  pour vous porter assistance.
                </p>
            </div>
            <div class="col-lg-6">
              <div class="img-header">
              <img src={ImgContact} class="img-fluid" alt="Contact-kryptonite" />
              </div>
            </div>
          </div>
       </div>
      </section>
    );
}

export default ContactBanner;