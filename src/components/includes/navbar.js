import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import Logo from '../images/logo-kryptonite.d92a6e5c.png';
const Navbar = () => {
  const [show, setShow] = useState(false)
  const controlNavbar = () => {
    if (window.scrollY > 100) {
      setShow(true)
    }else {
      setShow(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', controlNavbar)
    return () => {
      window.removeEventListener('scroll', controlNavbar)
    };
  }, [])

  return (
    <div id="header-menu" className={`sticker-wrapper ${show && 'nav-wrapper'}`}>
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <Link class="navbar-brand" to="/"><img src={Logo} class="img-fluid" alt="logo-kryptonite"/></Link>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <Link class="nav-link" to="/">Accueil</Link>
              <Link class="nav-link" to="/Contact">Contact</Link>
              <li class="nav-item dropdown">
                <Link class="nav-link dropdown-toggle" to="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Nos produits
                </Link>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="https://app.kryptonite.africa/exchange">P2P Exchange</a></li>
                  <li><a href="https://app.kryptonite.africa/transfert" class="dropdown-item">Transfert</a></li>
                  <li><a class="dropdown-item" href="https://docs.google.com/forms/d/e/1FAIpQLSc7KPx7w3XPqGjAeEqX21rSIxonI71vPUgSMtQWM9m_-NuukQ/viewform">Kryptonite Pay</a></li>
                </ul>
              </li>
            </div>
          </div>
        </div> 
      </nav>
    </div>
  );
}


export default Navbar;