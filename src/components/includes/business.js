import React from "react";
import ImgBusiness from '../images/Kryptonite-Pay.png'
import CheckPoint from '../images/checklist.png';
import { Link } from "react-router-dom";


function Business() {
  return (
    <section id="Business" class="Business py-11">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <div class="text-business">
              <h2>Encaissez des paiements en cryptomonnaies en toute simplicité avec <span>Kryptonite Pay</span></h2>
              <p>
                Vous êtes commerçant ? Entrez dans une nouvelle ère avec Kryptonite Pay ! Encaissez désormais en ligne 
                et en point de vente des paiements en cryptomonnaies et effectuez vos retraits en devise locale ou en 
                stablecoin.
              </p>
              <div class="row">
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Collecte de paiements en BTC, ETH, USDT, BUSD et bien d’autres</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Support de wallets populaires comme Trustwallet, Metamask, Phantom, etc.</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Conversion automatique en stablecoin de votre choix</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Retraits en devise locale ou stablecoin</span>
                </div>
              </div>
            </div>
              <a class="btn" href="https://docs.google.com/forms/d/e/1FAIpQLSc7KPx7w3XPqGjAeEqX21rSIxonI71vPUgSMtQWM9m_-NuukQ/viewform">Obtenir un accès anticipé</a>
            </div>
            
          </div>
          <div class="col-md-5"><img src={ImgBusiness} class="img-fluid" alt="Kryptonite-Business" /></div>
        </div>
      </div>
    </section>
  );
}


export default Business;