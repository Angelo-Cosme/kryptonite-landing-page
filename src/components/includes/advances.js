import React from "react";
import Wallet from '../images/wallet.png';
import Tap from '../images/tap.png';
import PaymentGateway from '../images/payment-gateway.png';
import Network from '../images/network.png';
import BigData from '../images/big-data.png';
import Accessibility from '../images/africa.png';
import CheckPoint from '../images/checklist.png';
import { Link } from "react-router-dom";

function Advances () {
    return(
      <section id="advances" class="advances py-11">
      <div class="container">
       <div class="row">
         <div class="col-md-6"> 
         <div class="text-advance">
           <h2>Echangez vos <span>cryptomonnaies</span> en toute sécurité</h2>
             <p>
             Achetez et vendez les cryptomonnaies de votre choix parmi des centaines d’offres
            </p>
            <div class="row">
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Achetez et vendez des BTC, ETH, USDT, BUSD et bien d’autres</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Trouvez toujours le meilleur prix en comparant des centaines d’annonces</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Payez avec le moyen de paiement de votre choix: MoMo, Wave, etc.</span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="check-item">
                  <img src={CheckPoint} class="img-fluid mr-2" alt="check-Kryptonite" />
                  <span>Sécurisez vos échanges avec notre système de tiers de confiance Kryptonite Pay</span>
                </div>
              </div>
            </div>
             <a href="https://app.kryptonite.africa/exchange" class="btn">Commencer à trader</a>
         </div>
         </div>
         <div class="col-md-5 offset-1">
           <div class="row panel">
             <div class="col-md-4 col-sm-6 col-xs-6">
               <div class="item-advance first">
                 <img src={Wallet} class="img-fluid" alt="Wallet-Kryptonite" />
                 <p class="mt-2">Parserelles Locales</p>
               </div>
             </div>
             <div class="col-md-4 col-sm-6 col-xs-6">
               <div class="item-advance itm-1">
                 <img src={Tap} class="img-fluid" alt="Tap-Kryptonite" />
                 <p class="mt-2">Rapide & Fiable</p>
               </div>
               <div class="item-advance itm-2">
                 <img src={Network} class="img-fluid" alt="Netword-Kryptonite" />
                 <p class="mt-2">P2P Exchange</p>
               </div>
               <div class="item-advance itm-3">
                 <img src={BigData} class="img-fluid" alt="Big-data-Kryptonite" />
                 <p class="mt-2">Données Sécurisées</p>
               </div>
             </div>
             <div class="col-md-4 col-sm-6 col-xs-6">
               <div class="item-advance three">
                 <img src={PaymentGateway} class="img-fluid" alt="Payment-Gateway-Kryptonite" /> 
                 <p class="mt-2">Transfert instantané</p>
               </div>
               <div class="item-advance four">
                 <img src={Accessibility} class="img-fluid" alt="Accessibility-Kryptonite" />
                 <p class="mt-2">Basé sur la Blockchain</p>
               </div>
             </div>
           </div>
         </div>
       </div>
      </div>
    </section>
  );
}

export default Advances;