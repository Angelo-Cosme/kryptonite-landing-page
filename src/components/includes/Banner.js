import React from "react";
import { FaArrowRight } from 'react-icons/fa';
import ImgHeader from "../images/image-Header.png";


function Slider () {
    return(
    <section id="slider">
      <div class="container">
        <div class="row content">
          <div class="col-lg-6 col-md-12 col-sm-12 me-auto flex-column">
            <h1 class="mb-4"><span>Transférez</span> de l’argent et <span>tradez</span> des dizaines de cryptomonnaies entre particuliers en toute sécurité.</h1>
            <p>
              Kryptonite est la seule plateforme qui vous permet en étant en Afrique de recevoir et d’envoyer de l’argent partout dans le monde de façon rapide 
              et sécurisée et d’acheter ou de vendre des cryptomonnaies entre particuliers en utilisant des moyens de paiement locaux.
             </p>
              <a href="https://app.kryptonite.africa/signup" class="btn me-auto">Démarrer maintenant <FaArrowRight /></a>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 mx-auto align-items-center">
            <div class="img-header"><img src={ImgHeader} class="img-fluid float-end" data-aos="zoom-in-left" alt=""/></div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Slider;