import React from 'react';
import ContactBanner from '../includes/contactbanner';
import FormContact from '../includes/formcontact';



class Contact extends React.Component {
  render() {
    return(
      <div>
        <ContactBanner />
        <FormContact />
      </div>
    );
  }
}

export default Contact;
