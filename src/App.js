import './App.css';
import '../src/theme.css'; 

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Produits from './components/pages/Produits';
import Navbar from './components/includes/navbar';
import Footer from './components/includes/footer';

import "@fontsource/raleway";

function App() {
  return (
    <>
    <Router>
      <Navbar/>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="#" element={<About />} />
          <Route path="/Contact" element={<Contact />} />
          <Route path="#" element={<Produits />} />
        </Routes>
      <Footer/>
    </Router>
    </>
  );

}

export default App;
