import React from 'react';
import Slider from '../includes/Banner';
import Blockain from '../includes/blochain';
import Advances from '../includes/advances';
import SetUp from '../includes/setup';
import Business from '../includes/business'

class Home extends React.Component {
  render() {
    return(
      <div>
        <Slider/>
        <Blockain/>
        <Advances/>
        <SetUp/>
        <Business/>
      </div>
    ); 
  }
}

export default Home;
