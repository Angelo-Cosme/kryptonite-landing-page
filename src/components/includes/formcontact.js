import React from "react";
import { Link } from "react-router-dom";
import IconEmail from '../images/email.png';
import IconLocation from '../images/location.png';
import IconWhatsapp from '../images/whatsapp.png';


function FormContact () {
    return(
      <section id="Form" class="Form py-11">
        <div class="container">
          <div class="title text-center">
            <h2>Avez-vous Des Questions?</h2>
            <p>Ecrivez-nous par ce formulaire et nous vous répondrons en moins de 24h.</p>
          </div>
          <div class="row">
            <div class="col-lg-5">
              <div class="infos pt-8">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="contact-item">
                      <img src={IconLocation} class="img-fluid mr-4" alt="Contact-Icon" />
                      <span>Cotonou - Bénin</span>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="contact-item">
                      <img src={IconEmail} class="img-fluid mr-4" alt="Contact-Icon" />
                      <a href="mailto:support@kryptonite.africa">support@kryptonite.africa</a>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="contact-item">
                      <img src={IconWhatsapp} class="img-fluid mr-4" alt="Contact-Icon" /> 
                      <a href="tel:+44 7700 162104" class="mr-2">+44 7700 162104</a> /
                      <a href="tel:+229 90 01 59 26" class="ml-2">+229 90 01 59 26</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 pt-5">
              <div class="form-contact">
                <form action="https://formspree.io/f/xeqngeke" method="POST">
                  <div class="mb-4"><input type="text" name="name" class="form-control" placeholder="Nom & Prénom" /></div>
                  <div class="mb-4"><input type="email" name="email" class="form-control" placeholder="Adresse E-mail" /></div>
                  <div class="mb-4"><input type="text" name="subjet" class="form-control" placeholder="Objet" /></div>
                  <div class="mb-4"><textarea type="text" name="message" class="form-control" rows="3" placeholder="Votre Message"></textarea></div>
                  <button to="#" type="submit" class="btn form-btn float-end">Envoyer</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
}

export default FormContact;